Commerce Collect.js 
This module provides for processing payments in Drupal Commerce version 1.x (for 
D7) by using a payment gateway tokenization function known as "Collect.js" with 
a Direct Post API, greatly enhancing security with minimal impact on the 
end-user experience. This API was developed by Network Merchants, LLC and is 
used by other processors because NMI repackages the API for various other ISOs. 
If your payment gateway uses Collect.js, this module will work for you. 

Collect.js is a tokenization script. When the end-user clicks a button on the 
payment form, the payment processor generates and presents a payment form in a 
lightbox to the user, which only collects their CC number/expiration/security 
code. When submitted, Collect.js returns a payment token(a "nonce") to the 
payment form. This nonce is only valid for one-time use and expires in 24 hours 
if not used at all. Once the token is received, the payment is processed via the 
existing Direct Post API provided by the gateway. So what's the benefit? PCI-DSS 
standards consider a lightbox to be an entirely separate page, and since it is 
generated and served entirely by the gateway, this will result in SAQ-A 
compliance, with minimal impact on the user experience. 


INSTALLATION
Install, enable, and activate the payment method just as you would any other. 
You will need to adjust the payment method settings in your store configuration. 
In the settings page of your gateway, you will need to do the following: 

*Generate a new user, and grant access to the API functions. 
*Generate an API key for that user. 
*Generate a separate tokenization key for that same user. 

It's important that these are two separate keys and both are provisioned to the 
same user. If not, the gateway will reject the transactions. 

Copy and paste these these keys into the appropriate field. Also, set the URLs 
for the Direct Post API endpoint and the Collect.js external library. Save. The 
payment method will now be available on the checkout "payment" pane like others.

The external JS library must be available on page-load, so this payment method
must be set as the first/default option in your Drupal Commerce store. This can
be done by adjusting the "weight" property of your available payment methods. 


TESTING INFORMATION
Although there is a testing username and password available from the gateway, 
the use of this module requires you to generate a unique Tokenization Key, which 
is provisioned to a specific gateway username which you must create. Keys cannot 
be provisioned to the "demo" username, and therefore, any transactions attempted 
with the testing credentials will generate a critical failure since the 
usernames of the provisioned keys do not match the demo user. Using testing card 
numbers in a live gateway will similarly be rejected. 

Since all transactions with the test credentials will fail unconditionally, 
there's no point. Therefore, there are no options provided in the module 
settings for test transactions. 

The only way to fully test this module using testing card numbers and 
transactions is to: 
1)Set your gateway to "Test Mode" (found on the settings tab); and 
2)Use production-value API and tokenization keys. 

While in Test Mode, ALL transactions passed to your gateway, from ANY source, 
will NOT be processed. This means that if your gateway processeses transactions 
from other sources besides your Drupal Commerce store, transactions from those 
sources will ALSO not be processed. Turning Test Mode on and off is a trivial 
step and the change is instantaneous, but testers should take care to use Test 
Mode for brief periods of time during off-peak shopping hours for your gateway, 
if it is currently in-use for live transactions. If a real transaction from an 
end-user is passed to the gateway while in test mode, the result is extremely 
deceiving: the gateway will return SUCCESS messages to the source, which will 
result in checkout completion workflows being triggered, and will make customers 
think the transaction processed, etc. even though nothing actually happened. 
What's worse, there is no way to convert a Test Mode transaction into a real 
one. 

If your gateway is already live and processes large volumes of transactions, the 
impact of poor timing is potentially catastrophic. Testing should ONLY be 
performed by or in consultation with an individual familiar with the 
company/gateway shopping patterns and transaction volumes. Gateway 
Administrators should provision site builders/IT professionals carefully and 
avoid blindly granting permission to use Test Mode without fully briefing module 
testers on its implications, lest it be activated at 6am on Black Friday and 
remain active until the following Monday. 

Heed this advice and use common sense; you have been warned. 


IMPORTANT LIMITATIONS
*Since we never actually see or touch the any payment card data beyond the 
payment token, there is no data validation by Drupal before submission of the 
payment(other than to check that Collect.js returned a token of some kind). 
Merchants will need to review and verify their AVS and CVV rejection rules on 
their gateway's settings page. 

*Unable to create new Sale or Auth transactions from the Admin interface, 
but merchants can now Capture, Void, or Refund transactions right from their 
Drupal Commerce store. 

*No support for Recurring or Customer Vault operations. Since I do not use these
operations I have little interest in adding this functionality myself - patches
are welcome.


HELP WANTED
Ideas, tips, pointers, recommendations, patches, or offers of 
co-maintainership are welcome! 


CREDITS
Developed by TynanFox, originally for use on his own small ecommerce store, but
released on Drupal.org for the benefit of other small merchants, and to solicit
help with maintainence.
