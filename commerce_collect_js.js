document.addEventListener('DOMContentLoaded', function () {
  CollectJS.configure({
    // Sets the listener to the customer-facing "Continue" button.
    'paymentSelector' : '#edit-continue',
    'callback' : function(response) {
      // Return the token to our form's hidden element.
      document.getElementById("payment_token").value = response.token;
      // Submit the form.
      document.getElementById("commerce-checkout-form-review").submit();
    }
  });
});